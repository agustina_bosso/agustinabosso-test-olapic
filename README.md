# Agustinabosso test olapic

The purpose of this project is to use Protractor and Cucumber frameworks to run tests over Olapic sites in order to test some of their components.
Protractor along with Cucumber are oftenly used in AngularJS projects based in BDD.

## Requisites

* **NodeJS** 
* **Protractor** (npm install -g protractor)
* **Cucumber** (npm install -g cucumber)

## Installation

```
$ npm install
```

## Usage

```
$ npm run start-server
```

This will update and start an instance of a Selenium Server
 
## Tests

The following commands will run the Cucumber tests suites via Protractor:

### Carousel Widget suite

```
// Springfield site
$ protractor --baseUrl=http://myspringfield.com/ --suite CarouselWidget protractor.conf.js

// American Signature site
$ protractor --baseUrl=http://www.americansignaturefurniture.com/ --suite CarouselWidget protractor.conf.js
```

### Viewer Widget suite

```
// Springfield site
$ protractor --baseUrl=http://myspringfield.com/ --suite ViewerWidget protractor.conf.js

// American Signature site
$ protractor --baseUrl=http://www.americansignaturefurniture.com/ --suite ViewerWidget protractor.conf.js
```

### Gallery Widget suite

```
// Springfield site
$ protractor --baseUrl=http://myspringfield.com/ --suite GalleryWidget protractor.conf.js

// American Signature site
$ protractor --baseUrl=http://www.americansignaturefurniture.com/ --suite GalleryWidget protractor.conf.js
```

### Uploader Widget suite

```
// Springfield site
$ protractor --baseUrl=http://myspringfield.com/ --suite UploaderWidget protractor.conf.js

// American Signature site
$ protractor --baseUrl=http://www.americansignaturefurniture.com/ --suite UploaderWidget protractor.conf.js
```

## Reports

Once the suite has run an HTML report will be generated under Reports folder

## Bugs

Detailed on Bugs folder 

## Evidences

Attached on google drive folder:

https://drive.google.com/drive/folders/0B7_BbaEpKgh0VVQxbVNCTHB4REE




