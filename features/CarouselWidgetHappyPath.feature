Feature: Carousel Widget Happy path
  As an Anonymous user
  I want to access the Olapic Carousel Widget
  So that I can validate it has the correct amount of images

  Scenario: I want to validate the pre-established number of images in the Carousel Widget
    Given I am on the homepage as an Anonymous user
    Then I validate that the Carousel Widget has the correct amount of images
