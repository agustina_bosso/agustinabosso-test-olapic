Feature: Carousel Widget ID in URL
  As an Anonymous user
  I want to access the Olapic Carousel Widget
  So that I validate the images ID is contained in URL

Scenario: I want to validate that the Carousel Widget images IDs are contained in URL when the Viewer is opened and are not present when the Viewer is closed
  Given I am on the homepage as an Anonymous user
  And I stop the carousel animation
  Then I validate all the Carousel Widget images IDs are contained in the URL with proper format when the Viewer is opened


