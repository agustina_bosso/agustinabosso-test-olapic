Feature: Carousel Widget Looping
  As an Anonymous user
  I want to access the Olapic Carousel Widget
  So that I can validate the looping to the right and left is infinite

  Scenario: I want to validate that the image looping to the left on the Carousel Widget is infinite
    Given I am on the homepage as an Anonymous user
    And I stop the carousel animation
    When I move to the left repeatedly on the Carousel Widget
    Then I validate that eventually get the same image displayed on the Carousel Widget again

  Scenario: I want to validate that the image looping to the right on the Carousel Widget is infinite
    Given I am on the homepage as an Anonymous user
    And I stop the carousel animation
    When I move to the right repeatedly on the Carousel Widget
    Then I validate that eventually get the same image displayed on the Carousel Widget again