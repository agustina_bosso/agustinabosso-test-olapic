Feature: Carousel To Gallery
  As an Anonymous user
  I want to access the Olapic Carousel Widget
  So that I can validate its interaction with Gallery Widget Component

  Scenario: I want to validate that selecting the Gallery button the Gallery Widget is shown
    Given I am on the homepage as an Anonymous user
    And I stop the carousel animation
    When I select the Gallery option
    Then I validate the Gallery Widget is displayed