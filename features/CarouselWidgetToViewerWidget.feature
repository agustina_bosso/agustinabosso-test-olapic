Feature: Carousel Widget To Viewer Widget
  As an Anonymous user
  I want to access the Olapic Carousel Widget
  So that I can validate its interaction with Viewer Widget Component

  Scenario: I want to validate that clicking on the image opens the Viewer
    Given I am on the homepage as an Anonymous user
    And I stop the carousel animation
    When I select an image in the Carousel Widget
    Then I validate that the Viewer is displayed