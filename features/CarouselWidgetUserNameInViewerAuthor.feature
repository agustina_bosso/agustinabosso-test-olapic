Feature: Carousel Widget UserName In Viewer Author
  As an Anonymous user
  I want to access the Olapic Carousel Widget
  So that I can validate the image username is contained in the Viewer author

  Scenario: I want to validate that the text displayed when the Carousel image is hovered is contained in the Viewer author
    Given I am on the homepage as an Anonymous user
    And I stop the carousel animation
    When I select an image in the Carousel Widget
    Then I validate that the picture user name is contained in the Viewer author