Feature: Gallery Widget ID in URL
  As an Anonymous user
  I want to access the Olapic Gallery Widget
  So that I validate the images ID is contained in URL

  Scenario: I want to validate that the visible Gallery Widget images IDs are contained in URL when the Viewer is opened and are not present when the Viewer is closed
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    Then I validate that the visible Gallery Widget images IDs are contained in the URL with proper format when the Viewer is opened