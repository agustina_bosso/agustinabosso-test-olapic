Feature: Gallery Widget Load More Options Behavior
  As an Anonymous user
  I want to access the Olapic Gallery Widget
  So that I can validate Load more options button functionality

  Scenario: I want to validate that clicking selecting load more option adds new images to the Gallery Widget
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select the Load More option
    Then I see new images included in the Gallery Widget