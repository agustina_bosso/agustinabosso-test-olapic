Feature: Gallery Widget To Viewer Widget
  As an Anonymous user
  I want to access the Olapic Gallery Widget
  So that I can validate its interaction with Viewer Widget components

  Scenario: I want to validate that clicking on the image in Gallery Widget opens the Viewer
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select an image in the Gallery Widget
    Then I validate that the Viewer is displayed