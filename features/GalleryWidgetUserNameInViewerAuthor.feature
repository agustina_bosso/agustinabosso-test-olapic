Feature: Gallery Widget User Name In Viewer Author
  As an Anonymous user
  I want to access the Olapic Gallery Widget
  So that I can validate the image username is contained in the Viewer author

  Scenario: I want to validate that the text displayed when the Gallery image is hovered is contained in the Viewer author
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select an image in the Gallery Widget
    Then I validate that the picture user name is contained in the Viewer author




