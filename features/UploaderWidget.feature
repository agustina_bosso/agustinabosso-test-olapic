Feature: Uploader Widget feature
  As an Anonymous user
  I want to access the Olapic Uploader Widget
  So that I can validate its closing behavior

  Scenario: I want to validate that selecting the upload photo option opens the Upload modal and the ESC key closes it
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select the option to upload a new photo on the Gallery Widget
    Then I validate the Uploader is displayed
    And I validate the Uploader can be closed by clicking the close button

  Scenario: I want to validate that selecting the upload photo option opens the Upload modal and the ESC key closes it
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select the option to upload a new photo on the Gallery Widget
    Then I validate the Uploader is displayed
    And I validate the Uploader can be closed by pressing ESC key



