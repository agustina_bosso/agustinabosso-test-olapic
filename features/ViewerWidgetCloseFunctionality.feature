Feature: Viewer Widget Close Functionality
  As an Anonymous user
  I want to access the Olapic Viewer Widget
  So that I can validate its close behavior

  Scenario: I want to validate that validate the Viewer can be closed pressing ESC key and the image ID does not remain in the URL
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select an image in the Gallery Widget
    And I close the Viewer by pressing the ESC key
    Then I validate that the Viewer is not displayed
    And I validate the URL does not contain the image ID

  Scenario: I want to validate that validate the Viewer can be closed with the close button and the image ID does not remain in the URL
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select an image in the Gallery Widget
    And I close the Viewer by clicking the close button
    Then I validate that the Viewer is not displayed
    And I validate the URL does not contain the image ID