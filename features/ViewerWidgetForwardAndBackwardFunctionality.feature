Feature: Viewer Widget Forward and Backward functionality
  As an Anonymous user
  I want to access the Olapic Viewer Widget
  So that I can validate its interaction with other pages

Scenario: I want to validate the Viewer behavior when going forward and backward in the browser
Given I am on the homepage as an Anonymous user
And I stop the carousel animation
When I select an image in the Carousel Widget
Then I validate that the Viewer is displayed
When I select product from the Viewer Widget
Then I validate the URL does not contain the image ID
When I go back to the previous page
Then I validate that the Viewer is displayed
And I validate the URL contains the image ID
When I close the Viewer by clicking the close button
Then I validate the URL does not contain the image ID