Feature: Viewer Widget Looping
  As an Anonymous user
  I want to access the Olapic Viewer Widget
  So that I can validate the looping to the right and left is infinite

  Scenario: I want to validate that the image looping to the right on the Viewer Widget is infinite
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select an image in the Gallery Widget
    And I move to the right repeatedly on the Viewer Widget
    Then I validate that eventually get the same image displayed on the Viewer Widget again

  Scenario: I want to validate that the image looping to the left on the Viewer Widget is infinite
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select an image in the Gallery Widget
    And I move to the left repeatedly on the Viewer Widget
    Then I validate that eventually get the same image displayed on the Viewer Widget again