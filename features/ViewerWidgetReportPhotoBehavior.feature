Feature: Viewer Widget Report Photo Behavior
  As an Anonymous user
  I want to access the Olapic Viewer Widget
  So that I can validate Report photo section behavior

  Scenario: I want to validate the report photo behavior
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select an image in the Gallery Widget
    And I select the Report photo option on the Viewer Widget
    And I enter validemail@email.com value in email field
    And I enter Testing purposes value in reason field
    And I select Report button
    Then I validate the report process was successful

  Scenario: I want to verify the email format validation and cancel button functionalities
    Given I am on the homepage as an Anonymous user
    When I select the Gallery option
    And I select an image in the Gallery Widget
    And I select the Report photo option on the Viewer Widget
    And I enter %invalid.email-format& value in email field
    And I select Report button
    Then I see the Invalid email address! error message displayed
    When I select Cancel button
    Then I validate that the Report photo section is hidden
    When I select the Report photo option on the Viewer Widget
    Then I validate the error message is not displayed
