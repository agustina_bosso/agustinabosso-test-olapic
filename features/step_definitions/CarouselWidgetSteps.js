var BasePage = require('./../../pages/BasePage');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

var expect = chai.expect;

module.exports = function() {

    this.Given('I am on the homepage as an Anonymous user', function(next) {
        BasePage.get().then(function() {
            BasePage.maximize().then(function() {
                closeOverlayWidget();
            }).then(function() {
                next();
            });
        });
    });

    this.Given('I stop the carousel animation', function(next) {
        browser.wait(protractor.ExpectedConditions.elementToBeClickable(BasePage.getCarouselWidgetRightButton())).then(function() {
            BasePage.getCarouselWidgetRightButton().click().then(function() {
                browser.wait(protractor.ExpectedConditions.visibilityOf(BasePage.getCarouselImagesList().get(1))).then(function() {
                    browser.sleep(1500);
                });
            });
        }).then(function(){
            next();
        });
    });

    this.When('I move to the $direction repeatedly on the Carousel Widget', function(direction, next) {
        var imageListFirstItem = BasePage.getCarouselImagesList().first();

        imageListFirstItem.getAttribute('data-olapic-photo-id').then(function(ID) {
            firstItemID = ID;
        });

        BasePage.getCarouselImagesList().count().then(function(imageCount) {
            var directionButton;

            if (direction === 'right') {
                directionButton = BasePage.getCarouselWidgetRightButton();
            } else if (direction === 'left'){
                directionButton = BasePage.getCarouselWidgetLeftButton();
            }

            for(var i = 0; i < imageCount; i++){
                directionButton.click().then(function(){
                    browser.sleep(1500);
                });
            }
        }).then(function() {
            next();
        });
    });

    this.When('I select an image in the Carousel Widget', function(next) {
        BasePage.getCarouselImagesList().count().then(function(itemsNumber) {
            imagesListCount = itemsNumber;
        });

        var image = BasePage.getCarouselImagesList().get(1);

        browser.wait(protractor.ExpectedConditions.elementToBeClickable(image)).then(function(){
            image.getAttribute('data-olapic-photo-id').then(function(ID) {
                firstItemID = ID;
            });
            image.click().then(function() {
                image.getText().then(function(name) {
                    userName = name;
                });
            }).then(function() {
                next();
            });
        });
    });

    this.When('I select the Gallery option', function(next){
        var galleryButton = BasePage.getGalleryWidgetButton();

        galleryButton.sendKeys(protractor.Key.ENTER).then(function() {
            browser.wait(protractor.ExpectedConditions.visibilityOf(BasePage.getGalleryImagesList().get(0))).then(function() {
                next();
            });
        });
    });

    this.Then('I validate that eventually get the same image displayed on the Carousel Widget again', function(next) {
        var imageListFirstItem = BasePage.getCarouselImagesList().first();

        imageListFirstItem.getAttribute('data-olapic-photo-id').then(function(actualFirstItemID){
            expect(firstItemID).to.equal(actualFirstItemID);
        }).then(function() {
            next();
        });
    });

    this.Then('I validate that the Carousel Widget has the correct amount of images', function(next) {
        BasePage.getCarouselImagesList().count().then(function(imageCount) {
            var carouselClass = BasePage.getCarouselWidget().getAttribute('class');
            expect(carouselClass).to.eventually.contains(imageCount);
        }).then(function() {
            next();
        });
    });

    this.Then('I validate all the Carousel Widget images IDs are contained in the URL with proper format when the Viewer is opened', function(next) {
        BasePage.getCarouselImagesList().count().then(function(imageCount) {
            for (var i = 0; i < imageCount; i++) {
                var image = BasePage.getCarouselImagesList().get(1);

                image.getAttribute('data-olapic-photo-id').then(function(ID) {
                    firstItemID = ID;
                });
                image.click().then(function() {
                    browser.getCurrentUrl().then(function(url) {
                        this.currentUrl = url;
                    }).then(function() {
                        browser.wait(function() {
                            return browser.getCurrentUrl().then(function(url) {
                                if (url !== this.currentUrl){
                                    this.currentUrl = url;
                                    return true;
                                }
                            });
                        });
                    }).then(function() {
                        expect(browser.getCurrentUrl()).to.eventually.contains('#opi' + firstItemID);
                    });
                }).then(function() {
                    browser.actions().sendKeys(protractor.Key.ESCAPE).perform().then(function() {
                        browser.wait(function() {
                            return browser.getCurrentUrl().then(function(url) {
                                return url !== this.currentUrl;
                            });
                        }).then(function() {
                            expect(browser.getCurrentUrl()).to.eventually.not.contains('#opi' + firstItemID);
                        });
                    }).then(function() {
                        BasePage.getCarouselWidgetRightButton().click().then(function(){
                            browser.sleep(1500);
                        });
                    });
                });
            }
        }).then(function() {
            next();
        });
    });

    var closeOverlayWidget = function() {
        var modalCloseButton = BasePage.getOverlayWidgetCloseButton();

        modalCloseButton.isDisplayed().then(function() {
            modalCloseButton.click();
        }, defaultRejectedHandler);
    };

    var defaultRejectedHandler = function() {
    };
};