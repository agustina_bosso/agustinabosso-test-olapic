var BasePage = require('./../../pages/BasePage');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

var expect = chai.expect;

module.exports = function() {

    this.Then('I validate the Gallery Widget is displayed', function(next) {
        var galleryWidget = BasePage.getGalleryWidget();

        expect(galleryWidget.isDisplayed()).to.eventually.be.fulfilled.and.notify(next);
    });

    this.When('I select an image in the Gallery Widget', function(next) {
        var galleryWidget = BasePage.getGalleryWidget();

        browser.wait(protractor.ExpectedConditions.visibilityOf(galleryWidget)).then(function(){
            BasePage.getGalleryImagesList().count().then(function(itemsNumber) {
                imagesListCount = itemsNumber;
            });

            var image = BasePage.getGalleryImagesList().get(0);

            image.getAttribute('data-uniqueid').then(function(ID) {
                firstItemID = ID;
            });

            image.click().then(function() {
                image.getText().then(function(name) {
                    userName = name;
                });
            });
        }).then(function() {
            next();
        });
    });

    this.When('I select the Load More option', function(next){
        BasePage.getGalleryImagesList().count().then(function(imageCount){
            this.imageAmount = imageCount;
        }).then(function() {
            BasePage.getLoadMoreButton().click().then(function() {
                next();
            });
        });
    });

    this.Then('I see new images included in the Gallery Widget', function(next){
        BasePage.getGalleryImagesList().count().then(function(imageCount){
            this.newImageAmount = imageCount;
        }).then(function() {
            expect(this.newImageAmount > this.imageAmount).to.be.true;
        }).then(function() {
            next();
        });
    });

    this.Then('I validate that the visible Gallery Widget images IDs are contained in the URL with proper format when the Viewer is opened', function(next) {
        BasePage.getGalleryImagesList().map(function(image) {
            image.getAttribute('data-uniqueid').then(function(ID) {
                itemID = ID;
            });
            image.click().then(function() {
                browser.getCurrentUrl().then(function(url) {
                    this.currentUrl = url;
                }).then(function() {
                    browser.wait(function() {
                        return browser.getCurrentUrl().then(function(url) {
                            if (url !== this.currentUrl) {
                                this.currentUrl = url;
                                return true;
                            }
                        });
                    });
                }).then(function() {
                    expect(browser.getCurrentUrl()).to.eventually.contains('#opi' + itemID);
                });
            }).then(function() {
                browser.actions().sendKeys(protractor.Key.ESCAPE).perform().then(function() {
                    browser.wait(function() {
                        return browser.getCurrentUrl().then(function(url) {
                            return url !== this.currentUrl;
                        });
                    }).then(function() {
                        expect(browser.getCurrentUrl()).to.eventually.not.contains('#opi' + itemID);
                    }).then(function() {
                        browser.sleep(1500);
                    });
                });
            });
        }).then(function() {
            next();
        });
    });
};