var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
var BasePage = require('./../../pages/BasePage');

chai.use(chaiAsPromised);

var expect = chai.expect;

module.exports = function() {

    this.When('I select the option to upload a new photo on the Gallery Widget', function(next) {
        BasePage.getGalleryUploadButton().sendKeys(protractor.Key.ENTER).then(function() {
            next();
        });
    });

    this.When('I select the option to upload a new photo on the Carousel Widget', function(next) {
        browser.wait(function() {
            return BasePage.getCarouselUploadButton().isDisplayed();
        }).then(function(){
            BasePage.getCarouselUploadButton().sendKeys(protractor.Key.ENTER).then(function() {
                next();
            });
        });
    });

    this.Then('I validate the Uploader is displayed', function(next) {
        var uploader = BasePage.getUploaderWidget();

        browser.wait(protractor.ExpectedConditions.visibilityOf(uploader));
        expect(uploader.isDisplayed()).to.eventually.be.fulfilled.and.notify(next);
    });

    this.Then('I validate the Uploader can be closed by pressing ESC key', function(next) {
        var uploader = BasePage.getUploaderWidget();

        browser.actions().sendKeys(protractor.Key.ESCAPE).perform().then(function() {
            expect(uploader.isDisplayed()).to.eventually.be.false.and.notify(next);
        });
    });

    this.Then('I validate the Uploader can be closed by clicking the close button', function(next) {
        var uploader = BasePage.getUploaderWidget();

        BasePage.getUploaderCloseButton().click().then(function() {
            expect(uploader.isDisplayed()).to.eventually.be.false.and.notify(next);
        });
    });
};