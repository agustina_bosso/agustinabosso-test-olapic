var BasePage = require('./../../pages/BasePage');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

var expect = chai.expect;

module.exports = function() {

    this.When('I move to the $direction repeatedly on the Viewer Widget', function(direction, next) {
        var directionButton;

        if (direction === 'right'){
            directionButton = BasePage.getViewerWidgetRightButton();
        } else if (direction === 'left') {
            directionButton = BasePage.getViewerWidgetLeftButton();
        }

        browser.wait(function() {
            for(var i=0 ; i < imagesListCount ; i++) {
                browser.getCurrentUrl().then(function(url) {
                    this.currentUrl = url;
                }).then(function() {
                    browser.wait(protractor.ExpectedConditions.elementToBeClickable(directionButton)).then(function() {
                        directionButton.click().then(function() {
                            browser.wait(function() {
                                return browser.getCurrentUrl().then(function(url) {
                                    return url !== this.currentUrl;
                                });
                            });
                        });
                    });
                }).then(function() {
                    browser.sleep(1500);
                });
            }
            return true;
        }).then(function() {
            next();
        });
    });

    this.When('I select product from the Viewer Widget', function(next) {
        var firstProduct = BasePage.getViewerProductsList().get(0);

        firstProduct.click().then(function() {
            browser.wait(protractor.ExpectedConditions.invisibilityOf(BasePage.getViewerWidget())).then(function(){
                next();
            });
        });
    });

    this.When('I close the Viewer by clicking the close button', function(next){
        browser.wait(protractor.ExpectedConditions.visibilityOf(BasePage.getViewerCloseButton())).then(function(){
            BasePage.getViewerCloseButton().click().then(function() {
                var actualUrl;
                browser.getCurrentUrl().then(function(url) {
                    actualUrl = url;
                });
                browser.wait(function(){
                    return browser.getCurrentUrl().then(function(url) {
                        return url !== actualUrl;
                    });
                }, 1500).then(function(){}, function(){});
            }).then(function() {
                next();
            });
        });
    });

    this.When('I close the Viewer by pressing the ESC key', function(next){
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform().then(function() {
            next();
        });
    });

    this.When('I select the Report photo option on the Viewer Widget', function(next){
        browser.wait(protractor.ExpectedConditions.visibilityOf(BasePage.getViewerReportPhotoOption())).then(function(){
            BasePage.getViewerReportPhotoOption().click().then(function() {
                browser.wait(protractor.ExpectedConditions.visibilityOf(BasePage.getViewerReportPhotoEmailInput())).then(function(){
                    next();
                });
            });
        });
    });

    this.When('I go back to the previous page', function(next){
        browser.navigate().back().then(function() {
            next();
        });
    });

    this.When ('I enter $email value in email field', function(email, next) {
        BasePage.getViewerReportPhotoEmailInput().sendKeys(email).then(function() {
            next();
        });
    });

    this.When ('I enter $reason value in reason field', function(reason, next) {
        BasePage.getViewerReportPhotoReasonInput().sendKeys(reason).then(function() {
            next();
        });
    });

    this.When('I select Report button', function(next){
        BasePage.getViewerReportPhotoButton().click().then(function() {
            next();
        });
    });

    this.When('I select Cancel button', function(next){
        BasePage.getViewerCancelReportButton().click().then(function() {
            browser.sleep(1500);
        }).then(function() {
            next();
        });
    });

    this.Then('I see the $errorMessage error message displayed', function(errorMessage, next) {
        expect(BasePage.getViewerReportPhotoErrorMessage().getText()).to.eventually.contains(errorMessage).and.notify(next);
    });

    this.Then('I validate that the Viewer is displayed', function(next) {
        var viewer = BasePage.getViewerWidget();

        browser.wait(protractor.ExpectedConditions.visibilityOf(viewer));

        expect(viewer.isDisplayed()).to.eventually.be.fulfilled.and.notify(next);
    });

    this.Then('I validate that the picture user name is contained in the Viewer author', function(next) {
        browser.wait(protractor.ExpectedConditions.visibilityOf(BasePage.getViewerWidget()));
        BasePage.getAuthorName().getText().then(function(author) {
            expect(userName).to.contains(author);
        }).then(function() {
            next();
        });
    });

    this.Then('I validate the error message is not displayed', function(next) {
        expect(BasePage.getViewerReportPhotoErrorMessage().isDisplayed()).to.eventually.be.false.and.notify(next);
    });

    this.Then('I validate that the Report photo section is hidden', function(next) {
        BasePage.getViewerReportContainer().getAttribute('class').then(function(className) {
            expect(className).to.not.contains('open');
        }).then(function() {
            next();
        });
    });

    this.Then('I validate that eventually get the same image displayed on the Viewer Widget again', function(next) {
        expect(browser.getCurrentUrl()).to.eventually.contains(firstItemID).and.notify(next);
    });

    this.Then('I validate that the Viewer is not displayed', function(next) {
        expect(BasePage.getViewerWidget().isDisplayed()).to.eventually.be.false.and.notify(next);
    });

    this.Then('I validate the URL does not contain the image ID', function(next) {
        expect(browser.getCurrentUrl()).to.not.eventually.contains(firstItemID).and.notify(next);
    });

    this.Then('I validate the URL contains the image ID', function(next) {
        expect(browser.getCurrentUrl()).to.eventually.contains(firstItemID).and.notify(next);
    });

    this.Then('I validate the report process was successful', function(next){
        browser.sleep(1500).then(function() {
            expect(BasePage.getViewerReportSuccessfulMessage().isDisplayed()).to.eventually.be.true.and.notify(next);
        });
    });
};