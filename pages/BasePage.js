var OLAPIC_CAROUSEL_IMAGES_LIST = element.all(by.css('.olapic-carousel > li'));
var OLAPIC_CAROUSEL_UPLOAD_BUTTON = element(by.css('.olapic-upload > a'));
var OLAPIC_CAROUSEL_WIDGET = element(by.id('olapic_specific_widget'));
var OLAPIC_CAROUSEL_WIDGET_LEFT_BUTTON = element(by.className('olapic-nav-prev'));
var OLAPIC_CAROUSEL_WIDGET_RIGHT_BUTTON = element(by.className('olapic-nav-next'));
var OLAPIC_GALLERY_IMAGES_LIST = element.all(by.css('#olapic-wall > li'));
var OLAPIC_GALLERY_LOAD_MORE_BUTTON = element(by.className('olapic-loadmore'));
var OLAPIC_GALLERY_UPLOAD_BUTTON = element(by.css('a.olapic-upload'));
var OLAPIC_GALLERY_WIDGET  = element(by.className('olapic-wall-wrapper'));
var OLAPIC_GALLERY_WIDGET_BUTTON = element(by.css('.olapic-see-all > a'));
var OLAPIC_OVERLAY_WIDGET_CLOSE_BUTTON = element(by.className('ui-dialog-titlebar-close'));
var OLAPIC_UPLOADER_CLOSE_BUTTON = element(by.css('div[id^=olapicUploader] .close'));
var OLAPIC_UPLOADER_WIDGET = element(by.css('div[id^=olapicUploader] .modal-body'));
var OLAPIC_VIEWER_AUTHOR_NAME = element(by.className('author-realname'));
var OLAPIC_VIEWER_CANCEL_REPORT_BUTTON = element(by.css('.form-group.report-buttons > a:nth-child(1)'));
var OLAPIC_VIEWER_CLOSE_BUTTON = element(by.id('closeViewer'));
var OLAPIC_VIEWER_PRODUCTS_LIST = element.all(by.css('.products-list > li'));
var OLAPIC_VIEWER_REPORT_PHOTO_BUTTON = element(by.css('.form-group.report-buttons > input:nth-child(2)'));
var OLAPIC_VIEWER_REPORT_PHOTO_EMAIL = element(by.name('email'));
var OLAPIC_VIEWER_REPORT_PHOTO_ERROR_MESSAGE = element(by.id('reportInvalidEmail'));
var OLAPIC_VIEWER_REPORT_PHOTO_OPTION = element(by.className('report-photo'));
var OLAPIC_VIEWER_REPORT_PHOTO_REASON = element(by.name('reason'));
var OLAPIC_VIEWER_REPORT_SUCCESSFUL_MESSAGE = element(by.id('reportOK'));
var OLAPIC_VIEWER_REPORT_WRAPPER = element(by.className('olapic-report-wrapper'));
var OLAPIC_VIEWER_WIDGET = element(by.className('viewer-container'));
var OLAPIC_VIEWER_WIDGET_LEFT_BUTTON = element(by.id('viewer-prev'));
var OLAPIC_VIEWER_WIDGET_RIGHT_BUTTON = element(by.id('viewer-next'));

var BasePage = function() {

    this.get = function() {
        return browser.get(browser.baseUrl);
    };

    this.maximize = function() {
        return browser.driver.manage().window().maximize();
    };

    this.getCarouselWidget = function() {
        return OLAPIC_CAROUSEL_WIDGET;
    };

    this.getCarouselImagesList = function() {
        return OLAPIC_CAROUSEL_IMAGES_LIST;
    };

    this.getCarouselWidgetLeftButton = function() {
        return OLAPIC_CAROUSEL_WIDGET_LEFT_BUTTON;
    };

    this.getCarouselWidgetRightButton = function() {
        return OLAPIC_CAROUSEL_WIDGET_RIGHT_BUTTON;
    };

    this.getOverlayWidgetCloseButton = function() {
        return OLAPIC_OVERLAY_WIDGET_CLOSE_BUTTON;
    };

    this.getViewerWidget = function() {
        return OLAPIC_VIEWER_WIDGET;
    };

    this.getViewerCloseButton = function() {
        return OLAPIC_VIEWER_CLOSE_BUTTON;
    };

    this.getAuthorName = function() {
        return OLAPIC_VIEWER_AUTHOR_NAME;
    };

    this.getCarouselUploadButton = function () {
        return OLAPIC_CAROUSEL_UPLOAD_BUTTON;
    };

    this.getUploaderWidget = function() {
        return OLAPIC_UPLOADER_WIDGET;
    };

    this.getGalleryWidgetButton = function() {
        return OLAPIC_GALLERY_WIDGET_BUTTON;
    };

    this.getGalleryWidget = function () {
        return OLAPIC_GALLERY_WIDGET;
    };

    this.getUploaderCloseButton = function () {
        return OLAPIC_UPLOADER_CLOSE_BUTTON;
    };

    this.getGalleryUploadButton = function () {
        return OLAPIC_GALLERY_UPLOAD_BUTTON;
    };

    this.getGalleryImagesList = function() {
        return OLAPIC_GALLERY_IMAGES_LIST;
    };

    this.getLoadMoreButton = function() {
        return OLAPIC_GALLERY_LOAD_MORE_BUTTON;
    };

    this.getViewerWidgetLeftButton = function() {
        return OLAPIC_VIEWER_WIDGET_LEFT_BUTTON;
    };

    this.getViewerWidgetRightButton = function() {
        return OLAPIC_VIEWER_WIDGET_RIGHT_BUTTON;
    };

    this.getViewerProductsList = function() {
        return OLAPIC_VIEWER_PRODUCTS_LIST;
    };

    this.getViewerReportPhotoOption = function() {
        return OLAPIC_VIEWER_REPORT_PHOTO_OPTION;
    };

    this.getViewerReportPhotoEmailInput = function() {
        return OLAPIC_VIEWER_REPORT_PHOTO_EMAIL;
    };

    this.getViewerReportPhotoReasonInput = function() {
        return OLAPIC_VIEWER_REPORT_PHOTO_REASON;
    };

    this.getViewerCancelReportButton = function() {
        return OLAPIC_VIEWER_CANCEL_REPORT_BUTTON;
    };

    this.getViewerReportPhotoButton = function() {
        return OLAPIC_VIEWER_REPORT_PHOTO_BUTTON;
    };

    this.getViewerReportPhotoErrorMessage = function() {
        return OLAPIC_VIEWER_REPORT_PHOTO_ERROR_MESSAGE;
    };

    this.getViewerReportContainer = function() {
        return OLAPIC_VIEWER_REPORT_WRAPPER;
    };

    this.getViewerReportSuccessfulMessage = function() {
        return OLAPIC_VIEWER_REPORT_SUCCESSFUL_MESSAGE;
    };
};

module.exports = new BasePage;