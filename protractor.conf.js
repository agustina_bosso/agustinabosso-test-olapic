exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    getPageTimeout: 60000,
    allScriptsTimeout: 600000,

    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    Capabilities: [{
        'browserName': 'chrome',
        'shardTestFiles': true,
        'maxInstances': 1
    }],

    specs: [
        'features/*.feature'
    ],

    cucumberOpts: {
        format: 'pretty',
        profile: false,
        require: [
            'features/step_definitions/*.js',
            'support/*.js'
        ],
        tags: false,
        'no-source': true
    },

    resultJsonOutputFile: './Reports/report.json',

    suites: {
        GalleryWidget: ['features/GalleryWidgetUserNameInViewerAuthor.feature',
                        //Removed due to an issue with Chrome driver clicking on a div
                        // 'features/GalleryWidgetLoadMoreOptionsBehavior.feature',
                        'features/GalleryWidgetIDInUrl.feature',
                        'features/GalleryWidgetToViewerWidget.feature'],
        UploaderWidget: 'features/UploaderWidget.feature',
        ViewerWidget: ['features/ViewerWidgetForwardAndBackwardFunctionality.feature',
                        'features/ViewerWidgetLooping.feature',
                        'features/ViewerWidgetReportPhotoBehavior.feature',
                        'features/ViewerWidgetCloseFunctionality.feature'],
        CarouselWidget:['features/CarouselWidgetLooping.feature',
                        'features/CarouselWidgetIDInUrl.feature',
                        'features/CarouselWidgetUserNameInViewerAuthor.feature',
                        'features/CarouselWidgetToViewerWidget.feature',
                        'features/CarouselWidgetToGallery.feature',
                        'features/CarouselWidgetHappyPath.feature']
    }
};