var configure = function() {
    browser.ignoreSynchronization = true;
    this.setDefaultTimeout(200000);
}

module.exports = configure;