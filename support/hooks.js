var Cucumber = require('cucumber');
var conf = require('../protractor.conf').config;
var fs = require('fs');

var hooks = function () {
    "use strict";
    var outputDir = './Reports/';

    this.registerHandler('BeforeFeature', function (event) {
        return browser.get(browser.baseUrl);
    });

    var createHtmlReport = function (sourceJson) {
        var CucumberHtmlReport = require('cucumber-html-report');
        var report = new CucumberHtmlReport({
            source: sourceJson
            , dest: outputDir
        });
        report.createReport();
    };

    var JsonFormatter = Cucumber.Listener.JsonFormatter();
    JsonFormatter.log = function (string) {
        if (!fs.existsSync(outputDir)) {
            fs.mkdirSync(outputDir);
        }

        var targetJson = outputDir + 'report.json';
        fs.writeFile(targetJson, string, function (err) {
            if (err) {
                console.log('Failed to save cucumber test results to json file.');
                console.log(err);
            } else {
                createHtmlReport(targetJson);
            }
        });
    };

    this.registerListener(JsonFormatter);
}
module.exports = hooks;